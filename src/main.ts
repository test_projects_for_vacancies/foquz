import './style.scss'
import { observable, applyBindings, components } from "knockout"

// --- Components ---
import "./components/icon"

// --- Main component ---
function viewModel(params: { initialText: string }) {
  const text = observable(params.initialText)

  return {
    text,
  }
}

const template = `
<div id="app">
  <h1 data-bind="text: text()"></h1>
</div>`

components.register("main", {
  viewModel,
  template,
})

function rootModel() {
  return {
    context: "Hello, World!",
  }
}

applyBindings(rootModel)