import { components } from "knockout"

const template = `
<svg xmlns="http://www.w3.org/2000/svg" width="8" height="5" fill="none">
  <path fill="#06F" d="M3.54 4.47a.66.66 0 0 1 0-.93L6.87.2a.66.66 0 1 1 .94.94L4.48 4.47a.66.66 0 0 1-.94 0Z"/>
  <path fill="#06F" d="M4.47 4.47a.66.66 0 0 0 0-.93L1.13.19a.66.66 0 1 0-.94.94l3.34 3.34c.26.26.68.26.94 0Z"/>
</svg>`

components.register("icon-caret", { template })
