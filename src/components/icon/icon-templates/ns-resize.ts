import { components } from "knockout"

const template = `
<svg xmlns="http://www.w3.org/2000/svg" width="8" height="15" fill="none">
  <path fill="#8E9CBB" d="M4.7 14.7a1 1 0 0 1-1.42 0L.29 11.74a1 1 0 1 1 1.42-1.41l2.99 2.97a1 1 0 0 1 0 1.42Z"/>
  <path fill="#8E9CBB" d="M3.3 14.7a1 1 0 0 0 1.42 0l2.99-2.97a1 1 0 1 0-1.42-1.41L3.3 13.29a1 1 0 0 0 0 1.42Z"/>
  <path fill="#8E9CBB" d="M3.99 14.02a1 1 0 0 1-1-1V3a1 1 0 0 1 2 0v10.02a1 1 0 0 1-1 1Z"/>
  <path fill="#8E9CBB" d="M4.7.3a1 1 0 0 0-1.42 0L.29 3.26A1 1 0 1 0 1.71 4.7L4.7 1.7a1 1 0 0 0 0-1.42Z"/>
  <path fill="#8E9CBB" d="M3.3.3a1 1 0 0 1 1.42 0l2.99 2.97A1 1 0 1 1 6.29 4.7L3.3 1.7a1 1 0 0 1 0-1.42Z"/>
</svg>`

components.register("icon-ns-resize", { template })
