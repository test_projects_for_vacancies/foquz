import ko from "knockout"

export function icon(params: { iconName: string }) {
  const iconName = ko.observable(params.iconName)

  return {
    iconName,
  }
}

export default icon