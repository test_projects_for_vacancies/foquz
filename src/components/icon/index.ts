import ko from 'knockout'
import viewModel from './viewModel'
import template from './template.html?raw'
import "./icon-templates/index"

ko.components.register('icon', {
  viewModel,
  template,
})